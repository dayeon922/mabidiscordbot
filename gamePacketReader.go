package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"strings"
)

// kr 기준
const _GAME_SERVER = "211.218.233.0/24"

var _GAME_SERVER_PORTS = []string{"11020", "11021", "11023"}

const (
	OpcodeChat   = 0x526c
	OpcodeNotice = 0x526d
)

func gamePacketReaderLoop(ch chan<- *chatMsg) error {
	filter := fmt.Sprint("tcp and src net ", _GAME_SERVER)
	filter += fmt.Sprint(" and src port ( ", strings.Join(_GAME_SERVER_PORTS, " or "), " )")
	logger.Println("game packet filter...", filter)

	payloadCh, err := openNic(filter)
	if err != nil {
		return err
	}

	buffer := bytes.NewBuffer(nil)

	for {
		payload := <-payloadCh
		buffer.Write(payload)

		if conf.Debug {
			logger.Printf("game Packet Payload %x", payload)
		}

	readerLoop:
		for {
			msg, err := gamePacketReader(buffer)
			if err != nil {
				if err == io.EOF {
					break readerLoop
				}

				logger.Printf("game packet parse error %v", err)
				continue
			}

			if msg != nil {
				ch <- msg
			}
		}
	}
}

func gamePacketReader(buffer *bytes.Buffer) (*chatMsg, error) {
	// 헤더 읽기에 아직 부족
	headerSize := 6
	b := buffer.Bytes()
	if len(b) < 6 {
		return nil, io.EOF
	}

	length := le.Uint32(b[1:])

	// maybe
	flag := b[5]

	// ?
	if length == 0 || length > 0x10_0000 {
		buffer.Reset()
		return nil, nil
	}

	// 1일 때 op, id가 없음
	if flag != 0x03 {
		buffer.Next(int(length))
		return nil, nil
	}

	// too small
	if int(length) < headerSize+0xd {
		buffer.Next(int(length))
		return nil, ErrTooShortPacket
	}

	if buffer.Len() < int(length) {
		return nil, io.EOF
	}

	body := b[:int(length)]
	body = body[headerSize:]
	buffer.Next(int(length))

	op := Opcode(be.Uint32(body))
	id := be.Uint64(body[4:])
	body = body[0xc:]

	_, lenbytes := binary.Uvarint(body)
	body = body[lenbytes:]

	// unused
	_ = id

	switch op {
	case OpcodeChat:
		pkt, err := newGamePacket(bytes.NewReader(body))
		if err != nil {
			return nil, err
		}

		if len(pkt) < 3 || pkt[1].Type() != GamePacketElemString || pkt[2].Type() != GamePacketElemString {
			return nil, ErrInvalidPacketElem
		}

		// 거뿔만
		nick := pkt[1].Data().(string)
		if nick != "<ALL_CHANNELS>" {
			return nil, nil
		}

		return &chatMsg{
			// Nick: nick,
			Msg: pkt[2].Data().(string),
		}, nil

	case OpcodeNotice:
		pkt, err := newGamePacket(bytes.NewReader(body))
		if err != nil {
			return nil, err
		}

		if len(pkt) < 2 || pkt[0].Type() != GamePacketElemByte || pkt[1].Type() != GamePacketElemString {
			return nil, ErrInvalidPacketElem
		}

		// 집뿔만
		ttype := pkt[0].Data().(byte)
		if ttype != 0xd {
			return nil, nil
		}

		return &chatMsg{
			// Nick: "<ALL_CHANNELS>",
			Msg: pkt[1].Data().(string),
		}, nil
	}

	// opcode가 너무 많다
	// return nil, fmt.Errorf("chatPacketReader: unknown opcode %08x %x", op, body)
	return nil, nil
}
