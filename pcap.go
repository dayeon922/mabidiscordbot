package main

import (
	"fmt"
	"strings"

	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
)

func openNic(filter string) (<-chan []byte, error) {
	nic, clientIp := conf.Pcap.NicName, conf.Pcap.ClientIp
	handle, err := pcap.OpenLive(nic, 1024*1024, true, pcap.BlockForever)
	if err != nil {
		return nil, err
	}

	if clientIp != "" {
		filter += fmt.Sprint(" and dst host ", clientIp)
	}

	if err := handle.SetBPFFilter(filter); err != nil { // optional
		return nil, err
	}

	ch := make(chan []byte, 100)
	ps := gopacket.NewPacketSource(handle, handle.LinkType())

	go func() {
		for packet := range ps.Packets() {
			tl := packet.TransportLayer()
			if tl == nil {
				// empty (ack?)
				continue
			}

			payload := tl.LayerPayload()
			if len(payload) == 0 {
				continue
			}

			ch <- payload
		}
	}()

	return ch, nil
}

func printNicList() {
	nics, err := pcap.FindAllDevs()
	if err != nil {
		panic(err)
	}

	for _, nic := range nics {
		if len(nic.Addresses) == 0 {
			continue
		}

		ips := []string(nil)
		for _, v := range nic.Addresses {
			ips = append(ips, v.IP.String())
		}

		fmt.Println(nic.Name)
		if nic.Description != "" {
			fmt.Println("desc: ", nic.Description)
		}
		fmt.Println("ip: ", strings.Join(ips, ", "))
		fmt.Println()
	}
}
