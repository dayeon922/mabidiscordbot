package main

import (
	"golang.org/x/exp/constraints"
)

func getKeys[T constraints.Ordered, U any](m map[T]U) []T {
	keys := make([]T, 0, len(m))

	for k := range m {
		keys = append(keys, k)
	}

	return keys
}
