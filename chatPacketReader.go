package main

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
)

// 길드, 패밀리, 친구 접속 알림, 채팅, DM

const (
	// kr 기준
	_CHAT_SERVER      = "211.218.233.193"
	_CHAT_SERVER_PORT = "8002"
)

type Opcode uint32

const (
	OpcodeFriendListRes     = 0xc359
	OpcodeFriendOnline      = 0xc35e
	OpcodeFriendOffline     = 0xc35f
	OpcodeGuildChatMsg      = 0xc36f
	OpcodeGuildChatImageMsg = 0xc391
	OpcodeFamilyChatMsg     = 0xc3a0
	OpcodeFamilyJoin        = 0xc3a1 // maybe
)

var ErrInvalidPacketSignature = errors.New("invalid packet signature")
var ErrTooShortPacket = errors.New("too short packet")
var ErrUnknownMsgType = errors.New("unknown msg type")
var ErrInvalidPacketElem = errors.New("invalid packet elem")

func chatPacketReaderLoop(ch chan<- *chatMsg) error {
	filter := fmt.Sprint("tcp and src host ", _CHAT_SERVER, " and src port ", _CHAT_SERVER_PORT)
	logger.Println("chat packet filter...", filter)

	payloadCh, err := openNic(filter)
	if err != nil {
		return err
	}

	buffer := bytes.NewBuffer(nil)

	for {
		payload := <-payloadCh
		buffer.Write(payload)

		if conf.Debug {
			logger.Printf("chat Packet Payload %x", payload)
		}

	readerLoop:
		for {
			msg, err := chatPacketReader(buffer)
			if err != nil {
				if err == io.EOF {
					break readerLoop
				}

				logger.Printf("chat packet parse error %v", err)
				continue
			}

			if msg != nil {
				ch <- msg
			}
		}
	}
}

func chatPacketReader(buffer *bytes.Buffer) (*chatMsg, error) {
	// 헤더 읽기에 아직 부족
	headerSize := 3
	b := buffer.Bytes()
	if len(b) < 6 {
		return nil, io.EOF
	}

	// signature
	if b[0] != 0x55 {
		// 패킷이 꼬임
		logger.Printf("invalid packet signature %x", b)
		buffer.Reset()
		return nil, ErrInvalidPacketSignature
	}

	length, lenbytes := binary.Uvarint(b[3:])
	headerSize += lenbytes

	// too small
	if length < 0xd {
		buffer.Next(headerSize + int(length))
		return nil, ErrTooShortPacket
	}

	if buffer.Len() < headerSize+int(length) {
		return nil, io.EOF
	}

	body := b[headerSize : headerSize+int(length)]
	buffer.Next(headerSize + int(length))

	op := Opcode(be.Uint32(body))
	id := be.Uint64(body[4:])
	body = body[0xc+lenbytes:]

	// unused
	_ = id

	switch op {
	case OpcodeGuildChatMsg:
		pkt, err := newGamePacket(bytes.NewReader(body))
		if err != nil {
			return nil, err
		}

		if len(pkt) < 2 || pkt[0].Type() != GamePacketElemString || pkt[1].Type() != GamePacketElemString {
			return nil, ErrInvalidPacketElem
		}

		return &chatMsg{
			Nick: pkt[0].Data().(string),
			Msg:  pkt[1].Data().(string),
		}, nil

	// case OpcodeFamilyChatMsg:
	// 	// log only
	// 	pkt, err := newGamePacket(bytes.NewReader(body))
	// 	logger.Println("family chat msg", pkt, err)
	// 	return nil, nil

	case OpcodeGuildChatImageMsg:
		pkt, err := newGamePacket(bytes.NewReader(body))
		if err != nil {
			return nil, err
		}

		if len(pkt) < 2 || pkt[0].Type() != GamePacketElemString || pkt[1].Type() != GamePacketElemString {
			return nil, ErrInvalidPacketElem
		}

		return &chatMsg{
			Nick:   pkt[0].Data().(string),
			ImgUrl: pkt[1].Data().(string),
		}, nil

	case OpcodeFriendListRes, OpcodeFriendOnline, OpcodeFriendOffline, OpcodeFamilyChatMsg,
		OpcodeFamilyJoin:

		// nop
		return nil, nil
	}

	return nil, fmt.Errorf("chatPacketReader: unknown opcode %08x %x", op, body)
}
